<p align="center">
  <img src="assets/googlectf.png" width="350px"/></p>

# Google CTF 2018 - Beginners Quest

## Letter
This is the first challenge. It involved downloading the attached BIN file. I opened the bin file using an archive manager. It contains a PDF file with the flag. The flag is blacked out but can be seen by highlighting it to reveal the text.

## OCR is cool
After spending way too much time reading about `socat` and others for the **moar** challenge, I decided to focus on this instead. The attachment contains a `.png` file. The image contains scrambled text. The clue here in the challenge is
>Caesar once said

1. I searched on the recommended tool for OCR - came across `tesseract`
2. `tesseract OCR_is_cool.png ocriscool`
3. Found online tools to decode caesar ciphers - used [this](https://www.nayuki.io/page/automatic-caesar-cipher-breaker-javascript) which calculated all possible shifts
4. Found the flag at shift 19

## Floppy
An interesting `.ico` file is what we had. Used `hexdump` initially and didn't at first notice that there was a mention of another file in the output. Continued to dig further thinking the flag was encoded somehow. Tried using `strings` - no luck.

Apparently, I should have been looking for `PK` which indicates an embedded file. It was simple after this discovery:
```
binwalk -e foo.ico
```

CTF flag was found in `driver.txt`

## Security by obscurity
I firstly checked what type of file was given using the `file` command. It was a compressed archive. I proceeded to unarchive the first level using `7z`. Unfortunately, not being proficient with a scripting language bit my arse on this one as  I had to manually extract each level. Rest assured I will be brushing up my bash skills to make sure nothing like this happens again 😉

The final archive file required a password. I tried all common passwords with no luck. Proceeded to search for zip password cracking utilities and decided to use [`fcrackzip`](https://allanfeid.com/content/cracking-zip-files-fcrackzip):

```
fcrackzip -v -m 1 -l 4-8 -u password.x
```
The password was `asdf` 😐

----
*Note, after this point I wasn't able to fully solve challenges myself. I may have come close but I needed to use external writeups and help from the IRC to get the flags. References are provided*

## Moar
I spent quite a bit of time on this. I read up on `socat`, tried a few commands to see if connecting to the server using what was provided in the man page would help me get the flag. After watching [Gynvael's CTF walkthrough](https://youtu.be/qDYwcIf0LZw?t=2641) what was actually needed was to manipulate the `man` page itself and use something like `!cat /home/moar/<flag name>` to find the flag. I honestly did not know that `man` could be manipulated this way.

At least I learnt about `socat` :smile:

## floppy2
A `.COM` file is provided as the attachment. I read the wiki file and found it needed to be run on DOS. I proceeded to install `dosbox` on kali and tried running the file. It was clear that this file will need to be debugged to find the flag. Unfortunately, I did not think of compiling dosbox with the debugger enabled. I tried using a community made `DEBUG.COM`. To be honest, my inexperience with reverse engineering meant that even with a proper debugger I would have no clue how to proceed. I found a really elegant solution on [this blog here [Korean]](https://qiita.com/kusano_k/items/fae67d401d8ad5006109#floppy2-misc) which simply involved using the `type` utility in FreeDOS to get the flag.

## JS Safe 1
Other than identifying the encryption function and reading up on `AES-CBC` I didn't get too far. I watched [GynvaelEN's](https://www.youtube.com/watch?v=qDYwcIf0LZw&t=4357s) walkthrough to extended my understand and see how the whole js program works. Really cool challenge!

## Admin UI
This was fun to learn about, again using the [walkthrough](https://www.youtube.com/watch?v=Eyzyj5B3EfU) provided by J. Hammond. This involved local file inclusion and it was a neat way to get the flag by exploiting a simple bug in the program.

## Gatekeeper
This was another **re** challenge. I got lucky when solving this one. I downloaded the attachment and ran `strings` on the file. At the top I could see 2 text inputs that kind of looked like they could be flags. The challenge text itself states
> You download the binary from the vendor and begin reversing it. Nothing is the right way around.

I performed `echo "text" | rev` to reverse the text and it turned out that was the flag. I am sure doing this the way it was "intended" would have taken quite some time and skill. I watched [John Hammond's](https://www.youtube.com/watch?v=bshuAGkgY3M) video to get a better understand of how this would have been properly reversed i.e. using some kind of disassembler
