# Attack Defence Online Labs

## Network Recon

### Memcached Recon: Basics

* `nmap` is a good tool to OSINT memcached
    * `memcached-info` is quite useful
* use telnet or nc to use text based commands such as `stats`
* `memcstat` for items
* Metasploit has a memcached module to alloow --> `msfconsole` and `memcached_extractor` module
* `memcdump` to get a list of all keys present
* To obtain a specific value stored, use `memcached-tool` (perl script)

### DNSSEC Enabled

Find ID of key signatures + algo

`dig @[dns_server] witrap.com DNSKEY +multiline`

RRSIG A records

`dig @[dns_server] witrap.com +dnssec +multiline`

Salt for NSEC3 hash calc.

`dig @[dns_server] witrap.com NSEC3PARAM`

RRSIG records for domain/s

`dig @[dns_server] witrap.com RRSIG`