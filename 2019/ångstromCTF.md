# ångstromCTF 2019

This was a very well designed CTF and I appliad the organisers for their hard work. 

I mainly woked on Misc challenges in my spare time of which I have shared my writeups below. Merge requests welcome :) 

<!-- MarkdownTOC -->

- [Misc](#misc)
	- [Streams](#streams)
	- [Scratch It Out](#scratch-it-out) 

<!-- /MarkdownTOC -->


## Misc

### Streams
>>>
70 points
White noise is useful whether you are trying to sleep, relaxing, or concentrating on writing papers. Find some natural white noise [here](https://streams.2019.chall.actf.co/).

Note: The flag is all lowercase and follows the standard format (e.g. actf{example_flag})

Author: ctfhaxor
>>>

1. https://streams.2019.chall.actf.co/ contains some images and a video stream
2. Initially tried `youtube-dl` and other stream downloaders with no luck
2. view source > download mp4 file
3. `file stream.mp4` reveals it is a xml MPD document
4. MPD contains data about MPEG DASH streams -- namely mpeg **streams** [^1]
5. There seem to be a total of three streams as seen on line 38 `<AdaptationSet id="2"`: 

    `id=0` video `id=1` audio `id=2` audio
6. By using devtools > network we can see that init streams and chunks of both stream 0 and stream 1 are being downloaded:
![stream1](2019/assets/img/stream1.png)
7. I simply followed the URI of these and replaced the names with `stream2` e.g. https://streams.2019.chall.actf.co/video/chunk-stream2-00001.m4s and https://streams.2019.chall.actf.co/video/init-stream2.m4s

8. All 9 stream2 chunks and 1 init stream were downloaded (all `.m4s` files)
9. Use this nifty one liner[^2] to piece these chunks together 

    ```bash
    cat init-stream2.m4s *.m4s > stream2.wav
    ```
10. This is morse code :zap:
11. After trying a few online audio decoders for morse code I gave up and did it by hand. I used audiacity to find the waveform and figure out the dashes and dots. Plugged it into an [online decoder](https://morsecode.scphillips.com/labs/decoder/)
![stream2](2019/assets/img/stream2.png)


12. flag :triangular_flag_on_post:

    ```
    actf{f145h_is_d34d_l0n9_l1v3_mp39_d45h}
    ```


### Scratch It Out
>>>
**Incomplete**

60 points
An oddly yellow cat handed me this [message](https://files.actf.co/397a7663cfc657bea92b8038eb2a27804ac75ba56b74e56572e57f00414fd43f/project.json) - what could it mean?

Author: innoviox
>>>

* Got really close on this one. I explored the `.json` file supplied and googled some references
* Scratch2 project files are what we are looking at
* I added the `project.json` within a zip file and gave it a `.sb2` extension. However, scratch2 on Arch Linux (obtained from aur) didn't quite open it right. 
* this was set up as a standard zip file as advised in the hint
* other writeups reveal that this import should have worked and the flag would have been obtained after playing the sequence ¯\\\_(ツ)\_/¯ 

--------
[^1]: https://www.brendanlong.com/the-structure-of-an-mpeg-dash-mpd.html
[^2]: https://stackoverflow.com/a/29175518/8487339 